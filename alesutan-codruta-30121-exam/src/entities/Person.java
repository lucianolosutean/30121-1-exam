package entities;

public class Person {
    private int id;
    private String name;
    private String address;


    public String getName() {
        return name;
    }

    public String getAddress() {
        return address;
    }

    public int getId() {
        return id;
    }
}

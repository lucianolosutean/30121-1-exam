package departaments;


import entities.Employee;
import entities.Programmer;

import java.util.ArrayList;

public class IT extends Departament {
    public ArrayList<Programmer> programmers = new ArrayList<>();

    @Override
    public void addEmployee(Employee e) {
        programmers.add((Programmer) e);
    }

    public void removeEmployee(Employee e)
    {
        programmers.remove(e);
    }

    @Override
    public void updateEmployee(Employee e) {

        int i;
        for(i=0;i<programmers.size();i++) {
            if(programmers.get(i).equals(e)) {
                if(programmers.get(i).isWorking()==true) {
                    programmers.get(i).work();
                    programmers.get(i).setWorking(false);
                    programmers.get(i).relax();
                }
                if(programmers.get(i).isWorking()==false) {
                    programmers.get(i).relax();
                    programmers.get(i).setWorking(true);
                    programmers.get(i).work();
                }
            }
        }
    }

    public Employee getEmployee (int id)
    {
        int i;
        for(i=0;i<programmers.size();i++)
        {
            if(programmers.get(i).getDepartamentId()==id)
                System.out.println("Angajatul cu id-ul "+programmers.get(i).getId()+" a fost gasit" );
            if(programmers.get(i).isWorking()==true)
                System.out.println(programmers.get(i).getAddress() + " "+programmers.get(i).getName()+ " "+programmers.get(i).getId()+" "+programmers.get(i).getDepartamentId()+" MUNCESTE");
            if(programmers.get(i).isWorking()==false)
                System.out.println(programmers.get(i).getAddress() + " "+programmers.get(i).getName()+ " "+programmers.get(i).getId()+" "+programmers.get(i).getDepartamentId()+" Ia o pauza");
        }
    return null;
    }
}
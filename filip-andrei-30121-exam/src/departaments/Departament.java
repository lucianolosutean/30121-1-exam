package departaments;


import entities.Employee;

public abstract class Departament {
    private int id;
    private String name;

    public abstract void addEmployee(Employee e);
    public abstract void removeEmployee(Employee e);
    public abstract void updateEmployee(Employee e);
    public abstract Employee getEmployee(int id);

}

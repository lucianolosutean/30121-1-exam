package departaments;

import entities.Accountant;
import entities.Employee;

import java.util.ArrayList;

public class Finance extends Departament {
    public ArrayList<Accountant> accountatns = new ArrayList<>();
    
    
    public void addEmployee(Employee e) {
        accountatns.add((Accountant) e);
    }

    public void removeEmployee(Employee e)
    {
        accountatns.remove(e);
    }

  
    public void updateEmployee(Employee e) {
        int i;
        for(i=0;i<accountatns.size();i++) {
            if(accountatns.get(i).equals(e)) {
                if(accountatns.get(i).isWorking()==true) {
                    accountatns.get(i).work();
                    accountatns.get(i).setWorking(false);
                    accountatns.get(i).relax();
                }
                if(accountatns.get(i).isWorking()==false) {
                    accountatns.get(i).relax();
                    accountatns.get(i).setWorking(true);
                    accountatns.get(i).work();
                }
            }
        }
        

    }

    public Employee getEmployee (int id)
    {
        int i;
        for(i=0;i<accountatns.size();i++)
        {
            if(accountatns.get(i).getId()==id)
                System.out.println("Angajatul cu id-ul "+accountatns.get(i).getId()+" a fost gasit" );
            if(accountatns.get(i).isWorking()==true)
                System.out.println(accountatns.get(i).getAddress() + " "+accountatns.get(i).getName()+ " "+accountatns.get(i).getId()+" "+accountatns.get(i).getDepartamentId()+" MUNCESTE");
            if(accountatns.get(i).isWorking()==false)
                System.out.println(accountatns.get(i).getAddress() + " "+accountatns.get(i).getName()+ " "+accountatns.get(i).getId()+" "+accountatns.get(i).getDepartamentId()+" Ia o pauza");
        }
    return null;
    }
}


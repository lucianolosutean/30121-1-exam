package entities;



import actions.WorkDay;
import actions.LunchBreak;


public abstract class Employee extends Person  implements WorkDay, LunchBreak{
    private boolean working;
    private int departamentId;
    public abstract void work();
    public abstract void relax();

    public boolean isWorking() {
        return working;
    }

    public void setWorking(boolean working) {
        this.working = working;
    }

    public int getDepartamentId() {
        return departamentId;
    }

    public void setDepartamentId(int departamentId) {
        this.departamentId = departamentId;
    }
}


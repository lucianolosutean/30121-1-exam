package departments;

import entities.Employee;
import entities.Programmer;
import java.util.ArrayList;
import java.util.Objects;

public class IT extends Department{
    private ArrayList<Programmer> programmers = new ArrayList<Programmer>();
    public void addEmployee(Employee e){
        Programmer programmer = new Programmer();
        programmers.add(programmer);
    }
    public void removeEmployee(Employee e){};
    public Employee getEmployee(int id){
        for (int i = 0; i < programmers.size(); i++){
            Programmer a = (Programmer) programmers.get(i);
            //if (a.id.equals(id)) {
             //   System.out.println(a.id);
            //}
            System.out.println(a.id);
        }
        return null;
    }
    public void updateEmployee(Employee e){}

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        IT it = (IT) o;
        return Objects.equals(programmers, it.programmers);
    }

    @Override
    public int hashCode() {
        return Objects.hash(programmers);
    }

    public ArrayList<Programmer> getProgrammers() {
        return programmers;
    }
}

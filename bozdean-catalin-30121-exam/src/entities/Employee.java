package entities;

import actions.LunchBreak;
import actions.WorkDay;

public abstract class Employee extends Person implements WorkDay, LunchBreak {
	protected static boolean working;
	protected static int departmentId;
	
	abstract public void work();
	abstract public void relax();
	
}

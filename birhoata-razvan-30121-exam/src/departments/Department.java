package departments;

import entitties.*;

public abstract class Department {
	private int id;
	private String name;
	
	public abstract void addEmployee();
	public abstract void removeEmployee();
	public abstract void updateEmployee();
	public abstract Employee getEmployee();
	
	
	public 	Department(int id,String name)
	{this.id = id;
	this.name = name;
	
	}
	
	public String getName()
	{return name;}
	
	public int getID()
	{return id;}

}

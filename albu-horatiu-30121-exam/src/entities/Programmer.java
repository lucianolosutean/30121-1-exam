package entities;

public class Programmer extends Employee {

    public Programmer(boolean working, int departmentId) {
        super(working, departmentId);
    }

    @Override
    public void work(){
        System.out.println("The Programmer is working.");
    }

    @Override
    public void relax(){
        System.out.println("The Programmer is relaxing.");
    }
}

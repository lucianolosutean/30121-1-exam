package entities;

import actions.LunchBreak;
import actions.WorkDay;

public class Employee extends Person implements LunchBreak, WorkDay {
    private boolean working;
    private int departmentId;

    public Employee(boolean working, int departmentId) {
        this.working = working;
        this.departmentId = departmentId;
    }

    @Override
    public void relax() {
        System.out.println("Employee is relaxing.");
        working = false;
    }

    @Override
    public void work() {
        System.out.println("Employee is working");
        working = true;
    }


}

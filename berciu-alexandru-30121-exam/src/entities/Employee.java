package entities;

import actions.LunchBreak;
import actions.WorkDay;

public abstract class Employee extends Person implements WorkDay, LunchBreak {
    private boolean working;
    private int departmentId;

    public abstract void work();

    public abstract void relax();

}

package entities;

import departments.Department;
import departments.Finance;
import departments.IT;

import java.util.ArrayList;

public class Company {
    public static Company company;
    ArrayList<Department> departments;
    private String name;
    private String address;

    public Company() {

    }

    public Company(String name, String address) {
        this.name = name;
        this.address = address;
    }

    public void initDepartments() {
        company = new Company("UTC-N", "Str. G. Baritiu 26-28");
        departments = new ArrayList<>();
        departments.add(new IT(1, "Automatica")); //adauga un dept de IT denumit Automatica
        departments.add(new Finance(2, "FSEGA")); //adauga un dept de Finance denumit FSEGA
    }
}

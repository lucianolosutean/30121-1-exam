package entities;

import main.Main;

public class Accountant extends Employee {

    public void work() {
        Main.write("Send accountant to work");
    }

    public void relax() {
        Main.write("Let accountant relax");
    }
}

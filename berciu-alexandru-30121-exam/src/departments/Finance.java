package departments;

import entities.Accountant;
import entities.Employee;

import java.util.ArrayList;

public class Finance extends Department {
    ArrayList<Accountant> accountants = new ArrayList<>();

    public Finance(int id, String name) {
        super(id, name);
    }

    public void addEmployee(Employee e) {
        accountants.add((Accountant) e);
    }

    public void removeEmployee(Employee e) {
        accountants.remove(e);
    }

    public Employee getEmployee(int id) {
        return accountants.get(id);
    }

    public void updateEmployee(Employee e) {
        accountants.set(accountants.indexOf(e), (Accountant) e);
    }
}

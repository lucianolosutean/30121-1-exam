package departments;

import entities.Employee;
import entities.Programmer;

import java.util.ArrayList;

public class IT extends Department {
    ArrayList<Programmer> programmers = new ArrayList<>();

    public IT(int id, String name) {
        super(id, name);
    }

    public void addEmployee(Employee e) {
        programmers.add((Programmer) e);
    }

    public void removeEmployee(Employee e) {
        programmers.remove(e);
    }

    public void updateEmployee(Employee e) {
        programmers.set(programmers.indexOf(e), (Programmer) e);
    }

    public Employee getEmployee(int id) {
        return programmers.get(id);
    }
}

package main;

import departments.Finance;
import entities.Company;

import java.io.*;

public class Main {
    public static void main(String[] args) throws IOException {
        Company company = new Company();
        int pId = 0; //pentru a retine numarul de elemente din programmers
        int aId = 0; //pentru a retine numarul de elemente din accountants
        Finance finance;
        company.initDepartments(); //initializeaza departamentele
        int id;
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in)); //citire de la tastatura
        System.out.println("1.Adaugare angajat in departament\n2.Sterge angajat din departament\n3.Editeaza un angajat\n4.Pune un angajat sa munceasca\n5.Trimite un angajat in pauza\n6.Exit\n");
        id = (int) reader.readLine().charAt(0); //citeste caracterul introdus de la tastatura si il converteste in intreg
        while (id != 6) {
            switch (id) { //in functie de valoarea introdusa se executa un anumit set de instructiuni
                case 1:
                    String s = new String();
                    s = read();
                    //IT it =new IT(); it.addEmployee((Employee)s)
                    pId++; //creste nr de elemente din programmers

                    break;
                case 2:
                    String s1 = new String();
                    s1 = read();
                    //IT it =new IT(); it.removeEmployee((Employee)s)
                    pId--; //scade nr de elemente din programmmers
                    break;
                case 3:
                    String s2 = new String();
                    s2 = read();
                    //IT it =new IT(); it.updateEmployee((Employee)s)
                    break;
                case 4:
                    String s3 = new String();
                    s3 = read();
                    //Programmer pr =new Programmer(); pr.work()
                    break;
                case 5:
                    String s4 = new String();
                    s4 = read();
                    //Programmer pr =new Programmer(); pr.lunch()
                    break;
            }
            id = (int) reader.readLine().charAt(0);
        }
    }

    public static String read() { //pentru a citi din fisier
        String s = new String();
        try {
            BufferedReader reader = new BufferedReader(new FileReader(".\\30121-1-exam\\File\\Input"));
            //citeste din fisierul Input.txt si returneaza un String cu continutul din fisier
            String s1 = new String();
            while ((s1 = reader.readLine()) != null)
                s = s + s1;

            reader.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return s;
    }

    public static void write(String s) { //pt a scrie in fiser
        try {
            BufferedWriter writer = new BufferedWriter(new FileWriter(".\\30121-1-exam\\File\\Output", true));
            writer.append(' ');
            writer.append(s);
            //scrie in fisierul Output.txt string-ul pe care l-am dat ca parametru
            writer.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}

package departments;

import entities.Employee;

public abstract class Department {
    protected int id;
    protected String name;

    abstract void addEmployee(Employee e);
    abstract void removeEmployee(Employee e);
    abstract void updateEmployee(Employee e);
    abstract Employee getEmployee(int id);
}

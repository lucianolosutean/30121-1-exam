package entities;

import actions.LunchBreak;
import actions.WorkDay;

public abstract class Employee extends Person implements WorkDay, LunchBreak {

    protected boolean working;
    protected int departmentId;

    abstract public void work();
    abstract public void relax();

}

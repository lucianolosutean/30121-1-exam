package entities;

import java.io.*;

public class Company {
    private String name;
    private String adress;

    public Company(String name, String adress){
        this.adress=adress;
        this.name=name;
    }
    public void save(String fileName) {
        try {
            ObjectOutputStream o = new ObjectOutputStream(new FileOutputStream(fileName));
            o.writeObject(this);
            System.out.println("Compania este salvata :" + fileName);
        } catch (IOException e) {
            System.err.println("Compania nu este salvata.");
            e.printStackTrace();
        }
    }

    public static Company read(String fileName) {
        Company c = null;
        try {
            ObjectInputStream o = new ObjectInputStream(new FileInputStream(fileName));
            c = (Company) o.readObject();
            System.out.println("Grupul a fsot citit" + fileName);
        } catch (IOException e) {
            System.err.println("Grupul nu a fost citit");
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        return c;
    }
}
}

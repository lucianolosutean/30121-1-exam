package entities;

public class Accountant extends Employee {
    public Accountant(boolean working, int departmentId) {
        super(working, departmentId);
    }

    @Override
    public void work(){
        System.out.println("Angajatul lucreaza");
    }

    @Override
    public void relax(){
        System.out.println("Angajatul se relaxeaza");
    }
}

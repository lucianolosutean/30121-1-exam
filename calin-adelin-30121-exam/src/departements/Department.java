package departements;

import entities.Employee;

public class Department {
    private int id;
    private String name;

    public Department(int id, String name) {
        this.id = id;
        this.name = name;
    }

    public void addEmployee(Employee e){
        System.out.println("Angajatul a fost adaugat.");
    }

    public void removeEmployee(Employee e){
        System.out.println("Angajatul a fost sters.");
    }

    public void updateEmployee(Employee e){
        System.out.println("Angajatul a fost updatat");
    }

    public void getEmployee(int id){

    }
}

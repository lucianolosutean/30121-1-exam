package departements;

import entities.Employee;

public class IT extends Department {
    public IT(int id, String name) {
        super(id, name);
    }

    @Override
    public void addEmployee(Employee e){
        System.out.println("Angajatul a fost adaugat");
    }

    @Override
    public void removeEmployee(Employee e){
        System.out.println("Angajatul a fost sters");
    }

    @Override
    public void updateEmployee(Employee e){
        System.out.println("Angajatul a fost updatat.");
    }

    @Override
    public void getEmployee(int id){

    }
}

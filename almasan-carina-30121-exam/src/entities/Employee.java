package entities;

import actions.LunchBreak;
import actions.WorkDay;

public abstract class Employee extends Person implements LunchBreak, WorkDay {
    private boolean working;
    private int departmentId;

    public Employee(int id, String name, String adresa) {
        super(id, name, adresa);
    }

    public boolean isWorking() {
        return working;
    }

    public void setWorking(boolean working) {
        this.working = working;
    }

    public int getDepartmentId() {
        return departmentId;
    }

    public void setDepartmentId(int departmentId) {
        this.departmentId = departmentId;
    }

    public   abstract void work();

    public abstract  void relax();
}

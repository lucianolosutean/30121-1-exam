package entities;

public class Person {
    private int id;
    private String name;
    private String adresa;

    public Person(int id, String name, String adresa) {
        this.id = id;
        this.name = name;
        this.adresa = adresa;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAdresa() {
        return adresa;
    }

    public void setAdresa(String adresa) {
        this.adresa = adresa;
    }

    @Override
    public String toString() {
        return "Person{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", adresa='" + adresa + '\'' +
                '}';
    }
}

package entities;

import departments.Department;
import departments.Finance;
import departments.IT;

public class Company extends Department {
    private String name;
    private String adresa;
    public static IT IT_dep;
    public static Finance Finance_dep;

    public Company(int id, String name) {
        super(id, name);
    }


    public  void initDepartments(){
        IT IT_dep= new IT((int) 0.0,"departament de it");
        Finance_dep=new Finance(1, "departament de finante");

    }

    @Override
    public void addEmployee(Employee e) {

    }

    @Override
    public void removeEmployee(Employee e) {

    }

    @Override
    public void updateEmployee(Employee e) {

    }

    @Override
    public void getEmployee(int id) {

    }

    public String getName() {
        return name;
    }


    public String getAdresa() {
        return adresa;
    }

    public void setAdresa(String adresa) {
        this.adresa = adresa;
    }
}

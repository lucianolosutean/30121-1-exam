package departments;

import entities.Employee;

public  abstract class Department {
    private int id;
    private String name;

    public Department(int id, String name) {
        this.id = id;
        this.name = name;
    }

    public Department() {

    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public abstract void addEmployee(Employee e);
    public  abstract void removeEmployee(Employee e);
    public abstract void updateEmployee(Employee e);
    public abstract void getEmployee(int id);

}

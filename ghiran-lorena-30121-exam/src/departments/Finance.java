package departments;

import entities.Employee;


import java.util.ArrayList;
import java.util.List;
import java.util.Objects;


public class Finance extends Department {
    List emp=new ArrayList();

    public void addEmployee(Employee e){
        emp.add(e);
    }
    public void removeEmployee(Employee e){
        emp.remove(e);
    }
    public Employee getEmployee (int id){
        System.out.println(emp.get(id));
        return null;
    }
    public void updateEmployee(Employee e){
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Finance)) return false;
        Finance finance = (Finance) o;
        return Objects.equals(emp, finance.emp);
    }

    @Override
    public int hashCode() {
        return Objects.hash(emp);
    }
}


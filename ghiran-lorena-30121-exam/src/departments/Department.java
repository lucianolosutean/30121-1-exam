package departments;


import entities.Employee;

abstract class Department {
    private int id;
    private String name;

    abstract void addEmployee(Employee e);
    abstract void removeEmployee(Employee e);
}

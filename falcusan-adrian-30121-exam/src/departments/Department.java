package departments;
import java.util.ArrayList;
import entities.*;

/**
 * Created by adrianfalcusan on 13/05/2019.
 */
abstract class Department {
    private int id;
    private String name;

    public Department() {
    }

    abstract void addEmployee();
    abstract void removeEmployee();
    abstract void updateEmployee();
    abstract void getEmployee();

}

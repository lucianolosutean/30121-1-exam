package actions;

/**
 * Created by adrianfalcusan on 13/05/2019.
 */
interface WorkDay {
    void work();
}

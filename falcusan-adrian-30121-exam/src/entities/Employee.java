package entities;
/**
 * Created by adrianfalcusan on 13/05/2019.
 */
abstract class Employee extends Person{
    private boolean working;
    private int departmentID;

    public  Employee(){

    }

    abstract void relax();
    abstract void work();

}

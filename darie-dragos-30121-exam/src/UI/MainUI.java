package UI;

import javax.swing.*;

import entities.Company;
import main.Main;

import java.awt.Font;
import java.io.*;

public class MainUI extends JFrame{
	JLabel user;
	JButton IT_Departament;
	JButton Finance_Departament;
	
	JMenu addNewEmployee;
	JMenu deleteEmployee;
	JMenu editEmployee;
	JMenu breakEmployee;
	
	
	public MainUI()
	{
		this.setLayout(null);
		
		initUI();
		addUIElements();
		
		
		
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setSize(450,200);
        setVisible(true);
	}
	
	private void initUI()
	{
		user = new JLabel(Main.getCompany().getName());
		user.setBounds(50, 10, 500, 50);
		user.setFont(new Font("Serif", Font.PLAIN, 25));

		IT_Departament = new JButton(Company.IT_Departament.getName());
		IT_Departament.setBounds(10,100,200,50);
		
		Finance_Departament = new JButton(Company.Finance_Departament.getName());
		Finance_Departament.setBounds(200,100,200,50);
	}
	
	private void addUIElements()
	{
		add(user);
		add(IT_Departament);
		add(Finance_Departament);
	}
}

package main;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

import UI.*;
import entities.Company;

public class Main {
	private static Company company;
	
	public static Company getCompany()
	{
		if (company == null)
		{
			company = new Company("Nume companie","Adresa companie");
			company.initDepartaments();
		}
		
		return company;
	}
	
	public static void main(String[] args) throws IOException
	{
		try {
			citireDinFisierScriereInFisier();
		}
		catch(Exception e)
		{
			System.out.print("Eroare!");
		}
		
		new MainUI();
	}
	
	private static void citireDinFisierScriereInFisier() throws IOException
	{
		String tst = "test";
        FileWriter writer = new FileWriter("C:\\Users\\Mihawai\\Desktop\\test.txt");
        BufferedWriter bw = new BufferedWriter(writer);
        bw.write(tst);

			
		File file = new File("C:\\Users\\Mihawai\\Desktop\\test.txt"); 
		  
		BufferedReader br = new BufferedReader(new FileReader(file)); 
		  
		String st; 
		while ((st = br.readLine()) != null) 
		  System.out.println(st); 
		  
		  
	}
}

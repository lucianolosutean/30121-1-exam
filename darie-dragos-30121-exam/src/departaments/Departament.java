package departaments;

import entities.*;

public abstract class Departament {
	private int id;
	private String name;
	
	public abstract void addEmployee(Employee e);
	public abstract void removeEmployee(Employee e);
	public abstract void updateEmployee(Employee e);
	public abstract Employee getEmployee(int id);
	
	public Departament(int id,String name)
	{
		this.id = id;
		this.name = name;
	}
	
	public String getName()
	{
		return name;
	}
	
	public int getID()
	{
		return id;
	}
}

package entities;

import departaments.*;

public class Company {
	private String name;
	private String address;

	public static IT IT_Departament; //fara singleton deoarece suntem siguri ca initializarea a fost facuta
	public static Finance Finance_Departament;
	
	public Company(String name,String address)
	{
		this.name = name;
		this.address = address;
	}
	
	public void initDepartaments()
	{
		IT_Departament= new IT(0,"Departamentul IT");
		Finance_Departament = new Finance(1,"Departamentul de finante");
	}
	
	public String getName()
	{
		return name;
	}
	
	public String getAddress()
	{
		return address;
	}
}

package entities;

import actions.*;

public abstract class Employee extends Person implements WorkDay,LunchBreak {
	private boolean working;
	private int departmentId;
	
	public abstract void work();
	public abstract void relax();
}
